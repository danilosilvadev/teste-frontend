import React from 'react';
import { Search, Grid } from 'semantic-ui-react';
import _ from 'lodash';
import ThemeSelector from '../themeSelector';

export default function ({
  state,
  state: { searchValue, searchResults, isLoading },
  actions,
  actions: { changeSearch, selectCoin, blurSearch },
}) {
  return (
    <Grid.Column>
      <h1 className="c-white">Cipto APP</h1>
      <div className="f f-justify-between">
        <Search
          loading={isLoading}
          onSearchChange={_.debounce(changeSearch, 500, { leading: true })}
          value={searchValue}
          results={searchResults}
          onResultSelect={(e, { result }) => {
            selectCoin(result);
          }}
          minCharacters={2}
          onBlur={blurSearch}
        />
        <ThemeSelector state={state} actions={actions} />
      </div>
    </Grid.Column>
  );
}
