import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import CoinHeader from './index';
import ProviderCoinPage from '../../pages/coinPage';
import * as mock from './mock';

const { input } = mock;

describe('CoinHeader Component tests', () => {
    let wrapper, provider;
    beforeEach(() => {
        provider = shallow(<ProviderCoinPage />);
        wrapper = shallow(
            <CoinHeader state={input} actions={provider.instance().actions} />
        );
    });

    it('should render CoinHeader snapshot correctly', () => {
        const tree = renderer.create(wrapper).toJSON();
        expect(tree).toMatchSnapshot();
    });
})