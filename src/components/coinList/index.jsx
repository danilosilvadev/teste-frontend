import React from 'react';
import { Grid, Header, Segment } from 'semantic-ui-react';
import CoinSelected from '../coinSelected';
import './style.scss';

export default function ({
  state,
  state: {
    data: { coins },
  },
}) {
  return (
    <React.Fragment>
      <Grid.Column style={{ color: 'white' }}>
        <CoinSelected state={state} />
      </Grid.Column>
      <Grid.Column>
        <Segment className="coins">
          <Header>Criptmoedas:</Header>
          <ul>
            {/* eslint-disable-next-line */}
            {coins.length > 0 ? coins.map(({ id, symbol, price_usd }) => (
              <li key={id} className="coins__item f f-justify-between">
                <span className="m-2">{symbol}</span>
                {/* eslint-disable-next-line */}
                <span className="m-2">{price_usd}</span>
              </li>
            ))
              : (
                <h1>
                  Desabilite o AdBlock, pois a URL da
                  API contém palavras que ativam o bloqueio,
                  e por isso você não está vendo uma lista aqui.
                </h1>
              )}
          </ul>
        </Segment>
      </Grid.Column>
    </React.Fragment>
  );
}
