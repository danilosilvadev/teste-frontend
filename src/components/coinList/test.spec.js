import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import CoinList from './index';
import * as mock from './mock';

const { input } = mock;

describe('CoinList Component tests', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <CoinList state={input} />
        );
    });

    it('should render CoinList snapshot correctly', () => {
        const tree = renderer.create(wrapper).toJSON();
        expect(tree).toMatchSnapshot();
    });
})