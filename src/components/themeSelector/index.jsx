import React from 'react';
import randomstring from 'randomstring';
import { CustomButton } from '../../utils/styledHelpers';

export default function ({
  state: {
    theme,
    theme: { primary, secondary, tertiary },
  },
  actions: { changeTheme },
}) {
  return (
    <div className="f f-align-center">
      <CustomButton
        onClick={() => changeTheme(primary.backgroundColor)}
        theme={theme}
        color={primary.backgroundColor}
        key={randomstring.generate()}
      />
      <CustomButton
        onClick={() => changeTheme(secondary.backgroundColor)}
        theme={theme}
        color={secondary.backgroundColor}
        key={randomstring.generate()}
      />
      <CustomButton
        onClick={() => changeTheme(tertiary.backgroundColor)}
        theme={theme}
        color={tertiary.backgroundColor}
        key={randomstring.generate()}
      />
    </div>
  );
}
