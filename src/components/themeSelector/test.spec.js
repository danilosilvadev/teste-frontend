import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import ThemeSelector from './index';
import ProviderCoinPage from '../../pages/coinPage';
import * as mock from './mock';

const { input } = mock;

describe('ThemeSelector Component tests', () => {
    let wrapper, provider;
    beforeEach(() => {
        provider = shallow(<ProviderCoinPage />);
        wrapper = shallow(
            <ThemeSelector state={input} actions={provider.instance().actions} />
        );
    });

    it('should render ThemeSelector snapshot correctly', () => {
        const tree = renderer.create(wrapper).toJSON();
        expect(tree).toMatchSnapshot();
    });
})