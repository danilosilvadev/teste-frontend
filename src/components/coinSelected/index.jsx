import React from 'react';
import _ from 'lodash';
import { formatCoinsKeys } from '../../middlewares';

export default function ({ state: { selectedCoin } }) {
  return (
    <React.Fragment>
      {!_.isEmpty(selectedCoin) ? (
        <div>
          <h1>Moeda Selecionada:</h1>
          <ul>
            {_.keys(selectedCoin).map(item => (
              <li key={item}>
                <h3 className="m-bottom-0 m-top-2">
                  {`${formatCoinsKeys(item)}:`}
                </h3>
                <strong>{selectedCoin[item]}</strong>
              </li>
            ))}
          </ul>
        </div>
      ) : (
        <h2>Realize uma busca</h2>
      )}
    </React.Fragment>
  );
}
