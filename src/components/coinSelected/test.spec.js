import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import CoinSelected from './index';
import * as mock from './mock';

const { input } = mock;

describe('CoinSelected Component tests', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <CoinSelected state={input} />
        );
    });

    it('should render CoinSelected snapshot correctly', () => {
        const tree = renderer.create(wrapper).toJSON();
        expect(tree).toMatchSnapshot();
    });
})