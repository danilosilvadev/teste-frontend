import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CoinPage from './pages/coinPage';
import './utils/sass/_index.scss';
import 'semantic-ui-css/semantic.min.css';

ReactDOM.render(<CoinPage className='app' />, document.getElementById('root'));
