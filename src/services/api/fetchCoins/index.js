import axios from 'axios';
import BASE_URL from '../baseURL';

const fetch = () => axios
  .get(`${BASE_URL}ticker/`)
  .then(({ data }) => data)
  .catch(error => console.error(error));

export default fetch;
