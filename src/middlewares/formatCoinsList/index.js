import { getCurrency } from '../../utils/js';

/* eslint-disable */
export default function (coins) {
  return coins ? coins.map(({
    name, id, symbol, price_usd,
  }) => ({
    title: name,
      id,
    symbol,
    price_usd: getCurrency(price_usd, 'en', 'usd'),
  })) : [];
}
