import * as mock from './mock.json';
import formatCoinsList from './index';

const { input, output } = mock;

describe('formatCoinsList test', () => {
  it('tests if formatCoinsList deliveries a clean object to components', () => {
    expect(
      formatCoinsList(input.slice(0, 5)).map(item => {
        delete item.price_usd;
        return item;
      })
    ).toEqual(
      output.slice(0, 5).map(item => {
        delete item.price_usd;
        return item;
      })
    );
  });
});
