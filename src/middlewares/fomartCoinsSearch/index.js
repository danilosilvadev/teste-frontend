export default function (value, coinsList) {
  const coins = coinsList
    .filter(({ title }) => title.toUpperCase().startsWith(value.toUpperCase()))
    .map(item => ({ ...item }));
  return coins;
}
