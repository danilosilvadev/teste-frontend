import * as mock from './mock.json';
import formatCoinsSearch from './index';

const { input, output } = mock;

describe('formatCoinsSearch test', () => {
  it('tests if formatCoinsSearch deliveries the right object', () => {
    expect(
      formatCoinsSearch(input.value, input.coins).map(item => {
        delete item.price_usd;
        return item;
      })
    ).toEqual(
      output.map(item => {
        delete item.price_usd;
        return item;
      })
    );
  });
});
