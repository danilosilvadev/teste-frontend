import _ from 'lodash';

export default function (value) {
  return _.upperFirst(value.replace(/_/g, ' '));
}
