import * as mock from './mock.json';
import formatCoinsKeys from './index';

const { input, output } = mock;

describe('formatCoinsKeys test', () => {
  it('tests if formatCoinsKeys deliveries the keys of selected coin', () => {
    expect(formatCoinsKeys(input)).toEqual(output);
  });
});
