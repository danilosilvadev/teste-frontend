const limitCarachters = (string, limit) => (string.length > limit ? `${string.substring(0, limit - 1)}...` : string);

export default limitCarachters;
