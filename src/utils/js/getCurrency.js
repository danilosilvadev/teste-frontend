const getCurrency = (value, lang, currency) => new Intl.NumberFormat(lang, {
  style: 'currency',
  currency,
}).format(value);

export default getCurrency;
