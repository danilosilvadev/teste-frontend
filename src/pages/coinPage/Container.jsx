import React from 'react';
import { Grid } from 'semantic-ui-react';
import { BodyBackground } from '../../utils/styledHelpers';
import { CoinList, CoinsHeader } from '../../components';

export default function ({
  state,
  state: {
    theme,
    backgroundColor,
  },
  actions,
}) {
  return (
    <React.Fragment>
      <BodyBackground
        theme={theme}
        backgroundColor={backgroundColor}
      />
      <Grid
        stackable
        divided="vertically"
        className="m-left-6 p-top-2 coin-page h-100 p-right-md-6"
      >
        <Grid.Row centered columns={1}>
          <CoinsHeader state={state} actions={actions} />
        </Grid.Row>
        <Grid.Row centered columns={2}>
          <CoinList state={state} actions={actions} />
        </Grid.Row>
      </Grid>
    </React.Fragment>
  );
}
