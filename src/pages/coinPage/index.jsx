/* eslint-disable */
import React, { Component } from 'react';
import Container from './Container';
import { theme } from '../../theme';
import fetchCoins from '../../services/api/fetchCoins';
import { formatCoinsList, formatCoinsSearch } from '../../middlewares';

export default class Provider extends Component {
  constructor(){
    super();
    this.state = {
      searchValue: '',
      searchResults: [],
      isLoading: false,
      selectedCoin: {},
      data: {
        raw: [],
        coins: [],
        keys: [],
      },
      theme,
      backgroundColor: theme.primary.backgroundColor,
    };
    this.actions = {
      changeSearch: this.handleChangeSearch = this.handleChangeSearch.bind(this),
      changeTheme: this.handleChangeTheme = this.handleChangeTheme.bind(this),
      selectCoin: this.handleSelectCoin = this.handleSelectCoin.bind(this),
      blurSearch: this.handleBlurSearch = this.handleBlurSearch.bind(this),
      fetch: this.handleFetch = this.handleFetch.bind(this)
    };
  }

  componentDidMount() {
    this.actions.fetch();
  }

  handleFetch () {
    fetchCoins().then(coins => {
      this.setState({ 
        data: {
          coins: formatCoinsList(coins),
          raw: formatCoinsList(coins),
        }
      })
    });
  }

  handleChangeSearch({ target: { value } }) {
    const { coins, raw } = this.state.data;
    this.setState({ 
      isLoading: true, 
      searchValue: value, 
      searchResults: formatCoinsSearch(value, raw),
    },
    () => {
      this.setState({
      data: {
        coins: this.state.searchResults,
        raw,
      }
    })});
  }

  handleBlurSearch() {
    const { raw } = this.state.data;
    this.setState({ isLoading: false, searchValue: '', data: { coins: raw, raw }});
  }

  handleSelectCoin(selectedCoin) {
    const { raw } = this.state.data;
    this.setState({ selectedCoin, searchValue: '', isLoading: false, data: { coins: raw, raw }});
  }

  handleChangeTheme(newBgColor) {
    this.setState({ backgroundColor: newBgColor })
  }

  render() {
    return (
      <Container state={this.state} actions={this.actions} className='h-100' />
    );
  }
}
