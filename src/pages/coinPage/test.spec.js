import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import * as mock from './mock';
import CoinPage from './Container';
import ProviderCoinPage from './index';

const { state, selectedCoinMock } = mock;

describe('CoinPage tests', () => {
  let wrapper;
  let provider;
  beforeEach(() => {
    provider = shallow(<ProviderCoinPage />);
    wrapper = shallow(
      <CoinPage state={state} actions={provider.instance().actions} />
    );
  });

  it('should trigger changeSearch action and test searchChange value at state', () => {
    provider.instance().actions.changeSearch({ target: { value: 'myValue' }});
    expect(provider.state().searchValue).toEqual('myValue');
    expect(provider.state().isLoading).toEqual(true);
  });

  it('should trigger changeTheme action and test backgroundColor value at state', () => {
    provider.instance().actions.changeTheme('black');
    expect(provider.state().backgroundColor).toEqual('black');
  });

  it('should trigger selectCoin action and test the click at searchResults', () => {
    provider.instance().actions.selectCoin(selectedCoinMock);
    expect(provider.state().selectedCoin).toEqual(selectedCoinMock);
    expect(provider.state().isLoading).toEqual(false);
    expect(provider.state().searchValue).toEqual('');
  });

  it('should trigger changeSearch action and test searchChange value at state', () => {
    provider.instance().actions.changeSearch({ target: { value: 'myValue' } });
    expect(provider.state().isLoading).toEqual(true);
    provider.instance().actions.blurSearch();
    expect(provider.state().isLoading).toEqual(false);
  });

  it('should render CoinPage snapshot correctly', () => {
    const tree = renderer.create(wrapper).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
